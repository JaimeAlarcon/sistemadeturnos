/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package videos;

/**
 *
 * @author Jaime Alarcón
 */
public class Video {
    private String nombre;
    private static String dir;

    public Video() {
        this.nombre="";
        this.dir="";
       
    }

    public Video(String nombre) {
        this.nombre = nombre;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public static String getDir() {
        return dir;
    }

    public static void setDir(String dir) {
        Video.dir = dir;
    }
    
    
    
}
