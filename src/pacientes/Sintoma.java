/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pacientes;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author Jaime Alarcón
 */
public class Sintoma {
    private String nombre;
    private int prioridad; 

    public Sintoma() {
        nombre="";
        prioridad=0;
    }

    public Sintoma(String nombre, int prioridad) {
        this.nombre = nombre;
        this.prioridad = prioridad;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getPrioridad() {
        return prioridad;
    }

    public void setPrioridad(int prioridad) {
        this.prioridad = prioridad;
    }
    public static List<Sintoma> ListaSintomas(String fileName) throws FileNotFoundException{
        List<Sintoma> lista= new LinkedList<>();
        try{
        File f = new File(fileName);
        Scanner s = new Scanner(f);
        while(s.hasNextLine()){
        String line = s.nextLine();
        //System.out.println(line);
        String[] linea=line.split("\\|");
        Sintoma pro= new Sintoma(linea[0],Integer.parseInt(linea[1]));
        lista.add(pro);
        
        }
 
        }catch(FileNotFoundException e){
            System.out.println("El archivo no existe...");
        }
        
        
        return lista;
    }
    
    
    
}
