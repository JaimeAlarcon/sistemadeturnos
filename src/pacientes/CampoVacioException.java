/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pacientes;

/**
 *
 * @author Jaime Alarcón
 */
public class CampoVacioException extends Exception{
    public CampoVacioException() {
    }

    public CampoVacioException(String message) {
        super(message);
    }

    public CampoVacioException(String message, Throwable cause) {
        super(message, cause);
    }

    public CampoVacioException(Throwable cause) {
        super(cause);
    }

    public CampoVacioException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
    
}