/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pacientes;

import java.util.LinkedList;
import java.util.List;
import medicos.Turno;

/**
 *
 * @author Jaime Alarcón
 */
public class Paciente {
    private String nombres;
    private String Apellidos;
    private String edad;
    private String genero;
    private Sintoma sintoma;
    private Turno turno ;
    

    public Paciente() {
        this.nombres=null;
        this.Apellidos=null;
        this.edad=null;
        this.genero=null;
        this.sintoma=null;
    }

    public Paciente(String nombres, String Apellidos, String edad, String genero, Sintoma sintoma) {
        this.nombres = nombres;
        this.Apellidos = Apellidos;
        this.edad = edad;
        this.genero = genero;
        this.sintoma = sintoma;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return Apellidos;
    }

    public void setApellidos(String Apellidos) {
        this.Apellidos = Apellidos;
    }

    public String getEdad() {
        return edad;
    }

    public void setEdad(String edad) {
        this.edad = edad;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public Sintoma getSintoma() {
        return sintoma;
    }

    public void setSintoma(Sintoma sintoma) {
        this.sintoma = sintoma;
    }

    public Turno getTurno() {
        return turno;
    }

    public void setTurno(Turno turno) {
        this.turno = turno;
    }
    
    
    
    
    
}
