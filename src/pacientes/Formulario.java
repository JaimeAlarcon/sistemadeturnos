/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pacientes;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javax.swing.JOptionPane;
import medicos.VentanaMedico;

/**
 *
 * @author Jaime Alarcón
 */
public class Formulario {
    
    FlowPane root;
    Scene pantalla;
    GridPane grid;
    Label nombres;
    TextField cNombres;
    Label apellidos;
    TextField cApellidos;
    Label edad; 
    TextField cEdad;
    Label genero;
    TextField cGenero;
    Label mensaje;
    Label sintoma;
    TextField cSintoma;
    ObservableList<String> nomSintomas;
    ListView<String> sintomas;
    Button registrar;
    Button salir;
    

    public Formulario() {
        iniciarYorganizar();
        
    }

    private void iniciarYorganizar() {
        root = new FlowPane();
        grid= new GridPane();
        nombres= new Label("Nombres:");
        cNombres=new TextField();
        apellidos= new Label("Apellidos");
        cApellidos= new TextField();
        edad= new Label("Edad");
        cEdad= new TextField();
        genero= new Label("Genero"); 
        cGenero= new TextField();
        sintoma= new Label("Sintoma");
        cSintoma= new TextField();
        mensaje= new Label();
        mensaje.setStyle("-fx-background-color: white ;-fx-textfill: red;");
        nomSintomas = FXCollections.observableArrayList( 
         "VÓMITO" , "FIEBRE" , "QUEMADURA 3ER GRADO" , "ASFIXIA" , "MORETON" , "GASES" , "RESFRIO","CONGESTIÓN NASAL","RESFRIADO LEVE"); 
        sintomas = new ListView<String>(nomSintomas) ;
        registrar= new Button("Registrar");
        salir= new Button("Salir");
        registrar.setOnAction((ActionEvent event) -> {
           
           try{
           File archivo= new File("Registros.txt");
           String linea="";
           //String locacion=(String) ubicchoiceBox.getSelectionModel().getSelectedItem();
               ///System.out.println(location);
           Paciente p= new Paciente();
           p.setNombres(cNombres.getText());
           p.setApellidos(cApellidos.getText());
           p.setEdad(cEdad.getText());
           p.setGenero(genero.getText());
           String sEscogido= null;
           sEscogido = sintomas.getSelectionModel().getSelectedItem();
           System.out.println(sEscogido);
           List<Sintoma> ls= Sintoma.ListaSintomas("sintomas.txt");
           Sintoma sintom= null;
           for(Sintoma s: ls){
               if(sEscogido.equalsIgnoreCase(sEscogido)){
                   sintom=s;}
           }
           p.setSintoma(sintom);
           System.out.println("SiNTOMA PACIENTE"+p.getSintoma().getNombre());
           //p.getSintoma().setNombre();
           linea+=cNombres.getText()+"|"+cApellidos.getText()+"|"+cEdad.getText()+"|"+cGenero.getText()+"|"+sEscogido;
           
           //System.out.println(cNombres.getText());
           if(p.getNombres().equals("")||p.getApellidos().equals("")||p.getEdad().equals("")||p.getGenero().equals("")||p.getSintoma().getNombre().equals("")){
               NullPointerException n= new NullPointerException();
               throw n;
           }
               
           VentanaMedico.pacientes.add(p);
           
           registrar(linea,archivo);
           mensaje.setText("");
           // Mensaje de Successful
           JOptionPane.showMessageDialog(null, "Datos Registrados!!!");
           limpiarCampos();
           
           /**System.out.println(calendario.getValue());
           if (grupoGenero.getSelectedToggle() != null) {
            RadioButton rb=(RadioButton)grupoGenero.getSelectedToggle();
           
           if(((RadioButton)grupoGenero.getSelectedToggle()).getText().equals("masculino"))
                System.out.println("Masculino");
              
            // System.out.println(groupGender.getSelectedToggle().getUserData().toString());
             

         }*/
           
           
           
           
           }/**catch(NumberFormatException e){
             System.out.println("Mensaje: "+e.getMessage());
               mensaje.setText("Ingrese un numero en edad");
               limpiarCampos();
         }*/ catch(NullPointerException e){
             ///e.printStackTrace();
                System.out.println("Mensaje: "+e.getMessage());
               mensaje.setText("debe LLenar todos los campos");
               limpiarCampos();
               //mensaje.setText("");
               
            } catch (FileNotFoundException ex) {
                Logger.getLogger(Formulario.class.getName()).log(Level.SEVERE, null, ex);
            }        
           
      });
      salir.setStyle("-fx-background-color: darkslateblue; -fx-textfill: white;");
   
           salir.setOnAction((ActionEvent e)->{
               Platform.exit();
           });
        
        // tamaño del gridpane 
      grid.setMinSize(500, 500) ; 
       
      //Espaciado Horizontal 
      
      grid.setPadding(new Insets(10, 10, 10, 10) );  
      
     
      grid.setVgap(5) ; 
      grid.setHgap(5) ;       
      
     
      grid.setAlignment(Pos.CENTER) ; 
       
      
      grid.add(nombres, 0, 0) ; 
      grid.add(cNombres, 1, 0) ; 
       
      grid.add(apellidos, 0, 1) ;       
      grid.add(cApellidos, 1, 1) ; 
      
      grid.add(edad, 0, 2) ; 
      grid.add(cEdad, 1, 2) ;       
      //grid.add(femenino, 2, 2) ; 
      grid.add(genero, 0, 3) ; 
      grid.add(cGenero, 1, 3) ;       
      //grid.add(no, 2, 3) ;  
       
      grid.add(sintoma, 0, 4) ; 
      grid.add(sintomas, 1, 4) ;       
      //grid.add(dotnetCheckBox, 2, 4) ;  
       
      grid.add(registrar, 2, 5) ; 
      grid.add(mensaje,1,8);
      grid.add(salir, 1, 5) ;      
       
      //grid.add(lubicacion, 0, 6) ; 
      //grid.add(ubicchoiceBox, 1, 6) ;    
       
      //grid.add(bregistrar, 2, 8) ;
      
      //mensaje.setText("Hola");
      
      //agregando colores a los componentes    
      registrar.setStyle("-fx-background-color: darkslateblue; -fx-textfill: white;"); 
       
      nombres.setStyle("-fx-font: normal bold 15px 'serif' ") ; 
      apellidos.setStyle("-fx-font: normal bold 15px 'serif' ") ; 
      edad.setStyle("-fx-font: normal bold 15px 'serif' ") ; 
      genero.setStyle("-fx-font: normal bold 15px 'serif' ") ; 
      sintoma.setStyle("-fx-font: normal bold 15px 'serif' ") ; 
      //lnivel.setStyle("-fx-font: normal bold 15px 'serif' ") ; 
      //lubicacion.setStyle("-fx-font: normal bold 15px 'serif' ") ;
      mensaje.setStyle("-fx-background-color: white ;-fx-textfill: red;");
      ///mensaje.setText("Hola);
       
    
      grid.setStyle("-fx-background-color: BEIGE;") ;       
       
  
      root= new FlowPane();
      //Agregando Gridpane al root
      root.getChildren().add(grid);
      pantalla = new Scene(root) ; 
          
          
         
    }

    private void registrar(String contenido, File archivo) {
        try (FileWriter escritor= new FileWriter(archivo); ){
            //escritor = new FileWriter(file);
            escritor.write(contenido);
            escritor.close();
        } catch (IOException ex) {
            Logger.getLogger(Formulario.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public Scene getPantalla() {
        return pantalla;
    }

    public void setPantalla(Scene pantalla) {
        this.pantalla = pantalla;
    }
    private void limpiarCampos(){
               cNombres.clear();
               cApellidos.clear();
               cEdad.clear();
               cGenero.clear();
               cSintoma.clear();
               
    }
    /**
    private Sintoma obtenerSintoma(String nSintoma) throws FileNotFoundException{
        List<Sintoma> ls= Sintoma.ListaSintomas("sintomas.txt");
        Sintoma sint = null;
        for(Sintoma s:ls){
            if(s.getNombre().equals(nSintoma)){
                sint=s;
            }
        }
        return sint ;
    }*/
        
    
    
}
