/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package medicos;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import pacientes.Sintoma;

/**
 *
 * @author Jaime Alarcón
 */
public class Medico {
    private String nombre;
    private String especialidad;
    private boolean isDisponible;
    public Medico() {
        nombre="";
        especialidad="";
        isDisponible=true;
        
    }

    public Medico(String nombre, String especialidad) {
        this.nombre = nombre;
        this.especialidad = especialidad;
        this.isDisponible=true;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getEspecialidad() {
        return especialidad;
    }

    public void setEspecialidad(String especialidad) {
        this.especialidad = especialidad;
    }

    public boolean isIsDisponible() {
        return isDisponible;
    }

    public void setIsDisponible(boolean isDisponible) {
        this.isDisponible = isDisponible;
    }
    public static List<Medico> cargarMedicos(String nombre){
        List<Medico> medicos =new LinkedList<>();
        try{
        File f = new File(nombre);
        Scanner s = new Scanner(f);
        while(s.hasNextLine()){
        String line = s.nextLine();
        //System.out.println(line);
        String[] linea=line.split("\\|");
        Medico pro= new Medico(linea[0],(linea[1]));
        medicos.add(pro);
        
        }
 
        }catch(FileNotFoundException e){
            System.out.println("El archivo no existe...");
        }
        return medicos;
    }
    
    
    
}
