/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package medicos;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

/**
 *
 * @author Jaime Alarcón
 */
public class Turno {
    private String codigo;
    private PuestoMedico puesto;
    public static List<Turno> turnos= new LinkedList<>();

    public Turno() {
        codigo=generarCodigo();
        puesto=null;
    }
    

    public Turno( PuestoMedico puesto) {
        this.codigo = generarCodigo();
        this.puesto = puesto;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public PuestoMedico getPuesto() {
        return puesto;
    }

    public void setPuesto(PuestoMedico puesto) {
        this.puesto = puesto;
    }
    
    private String generarCodigo(){
        String c="A B C D E F G H I J K L M N O P Q R S T U V W X Y Z";
        String[] abece= c.split(" ");
        String n="0 1 2 3 4 5 6 7 8 9";
        String[] num= n.split(" ");
        String codigo="";
        Random rl = new Random();
        Random rn= new Random();
        int alLetra= rl.nextInt(abece.length);
        //System.out.println(alLetra);
        int alNum= rn.nextInt(num.length);
        int alNum2= rn.nextInt(num.length);
        int alNum3= rn.nextInt(num.length);
        System.out.println(alNum);
        System.out.println(alNum2);
        System.out.println(alNum3);
        codigo+=abece[alLetra]+num[alNum]+num[alNum2]+num[alNum3];       
        
        return codigo;
    }
    
    protected static void generarTurnos(){
        for(int i=1; i<=VentanaMedico.pacientes.size() ; i++){
            Turno t= new Turno ();
            Turno.turnos.add(t);
            
        }
        
    }
}
