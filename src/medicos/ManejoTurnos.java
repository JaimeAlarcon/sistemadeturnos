/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package medicos;

import java.util.LinkedList;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Queue;
import pacientes.Paciente;

/**
 *
 * @author Jaime Alarcón
 */
public class ManejoTurnos {
    public static void Manejo(){
        List<Medico> docs= Medico.cargarMedicos("medicos.txt");
        Queue<Medico> medicos= new LinkedList<>();
        medicos.addAll(docs);
        PuestoMedico.generarPuesto();
        List<PuestoMedico> puestos= PuestoMedico.puestos;
        Turno.generarTurnos();
        List<Turno> turnos= Turno.turnos;
        PriorityQueue<Paciente> colaPac= new PriorityQueue<>((Paciente p1,Paciente p2)->(p1.getSintoma().getPrioridad()-p2.getSintoma().getPrioridad()));
        colaPac.addAll(VentanaMedico.pacientes);
        Queue<Medico> meds= new LinkedList<>();
        meds.addAll(docs);
        for(PuestoMedico pm: puestos){
            Medico m= meds.poll();
            pm.setMedAsignado(m);
        }
    }
    
}
