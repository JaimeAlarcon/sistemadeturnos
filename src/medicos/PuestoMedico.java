/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package medicos;

import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author Jaime Alarcón
 */
public class PuestoMedico {
    private int numeroPuesto;
    private Medico medAsignado;
    private boolean isVacio;
    public static List<PuestoMedico> puestos = new LinkedList<>(); 

    public PuestoMedico() {
        numeroPuesto=0;
        medAsignado=null;
        isVacio=true;
    }

    public PuestoMedico(int numeroPuesto, Medico medAsignado, boolean isVacio) {
        this.numeroPuesto = numeroPuesto;
        this.medAsignado = medAsignado;
        this.isVacio = true;
    }

    public int getNumeroPuesto() {
        return numeroPuesto;
    }

    public void setNumeroPuesto(int numeroPuesto) {
        this.numeroPuesto = numeroPuesto;
    }

    public Medico getMedAsignado() {
        return medAsignado;
    }

    public void setMedAsignado(Medico medAsignado) {
        this.medAsignado = medAsignado;
    }

    public boolean isIsVacio() {
        return isVacio;
    }

    public void setIsVacio(boolean isVacio) {
        this.isVacio = isVacio;
    }
    public static void generarPuesto(){
        for(int i=1;i<=10;i++){
            PuestoMedico p= new PuestoMedico();
            p.setNumeroPuesto(i);
            PuestoMedico.puestos.add(p);
        }
    }
    
    
}
