/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sistemadeturnos;

import javafx.application.Application;
import javafx.stage.Stage;
import pacientes.Formulario;
/**
 *
 * @author Jaime Alarcón
 */
public class SistemaDeTurnos extends Application {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Application.launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        Formulario f = new Formulario();
        PrincipalInterface p= new PrincipalInterface();
        primaryStage.setScene(p.getPantalla());
        //primaryStage.setScene(f.getPantalla());
        primaryStage.setTitle("Formulario");
        primaryStage.show();
    }
    
}
