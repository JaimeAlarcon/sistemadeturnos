/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sistemadeturnos;

import java.util.ArrayList;
import java.util.List;
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

/**
 *
 * @author Jaime Alarcón
 */
public class prueba extends Application{
    public static void main (String[] args){
        Application.launch(args);
        
        
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
         primaryStage.setTitle("TEST");
  Group root = new Group();
  Scene scene = new Scene(root, 400, 300, Color.WHITE);

  TableView tv1 = new TableView();
  TableView tv2 = new TableView();

  TableColumn<String, String> clmn = new TableColumn<>("Test");
  clmn.setPrefWidth(230);

  tv1.getColumns().addAll(clmn);
  tv2.getColumns().addAll(clmn);

  List<String> firstItems = new ArrayList<>();
  firstItems.add("ONE");
  firstItems.add("TWO");
  firstItems.add("THREE");
  tv1.setItems(FXCollections.observableArrayList(firstItems));

  List<String> secondItems = new ArrayList<>();
  secondItems.add("1");
  secondItems.add("2");
  secondItems.add("3");
  tv2.setItems(FXCollections.observableArrayList(secondItems));

  TabPane tabPane = new TabPane();
  BorderPane mainPane = new BorderPane();

  //Create Tabs
  Tab tabA = new Tab();
  tabA.setText("Tab A");
  tabA.setContent(tv1);
  tabPane.getTabs().add(tabA);

  Tab tabB = new Tab();
  tabB.setText("Tab B");
  tabB.setContent(tv2);
  tabPane.getTabs().add(tabB);

  mainPane.setCenter(tabPane);
  mainPane.prefHeightProperty().bind(scene.heightProperty());
  mainPane.prefWidthProperty().bind(scene.widthProperty());  
  root.getChildren().add(mainPane);
  primaryStage.setScene(scene);
  primaryStage.show();
    }
    
}
