/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sistemadeturnos;

import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;

/**
 *
 * @author Jaime Alarcón
 */
public class PrincipalInterface {
   BorderPane root;
   Pane contenedorVid;
   HBox turnoPuesto; 
   VBox contenedorTurnoPuesto;
   Label titulo;
   Label reloj;
   Scene Pantalla;
   Label turno;
   Label puesto;
   Label horarioAtencion;
   HBox titulos;
   

    public PrincipalInterface() {
        inciarYorganizar();
    }

    private void inciarYorganizar() {
        root= new BorderPane();
        contenedorVid= new Pane();
        turnoPuesto= new HBox();
        contenedorTurnoPuesto= new VBox();
        titulo= new Label("Atencion Pacientes");
        reloj= new Label("AQUI METER RELOJ");
        turno= new Label("Turnos");
        puesto= new Label("Puestos");
        root.setLeft(contenedorVid);
        horarioAtencion= new Label("Atendemos de Lunes a sabado hasta las 11:00 PM");
        titulo.setAlignment(Pos.TOP_RIGHT);
        root.setTop(titulo);
        reloj.setAlignment(Pos.TOP_RIGHT);
        root.setTop(reloj);
        root.setRight(contenedorTurnoPuesto);
        horarioAtencion.setAlignment(Pos.CENTER);
        root.setBottom(horarioAtencion);
        titulos= new HBox();
        titulos.getChildren().addAll(turno,puesto);
        titulos.setAlignment(Pos.TOP_CENTER);
        root.setRight(titulos);
        contenedorTurnoPuesto.getChildren().add(turnoPuesto);
        root.setRight(contenedorTurnoPuesto);
        
        Pantalla= new Scene(root,500,500);
        
        
        
    }

    public BorderPane getRoot() {
        return root;
    }

    public void setRoot(BorderPane root) {
        this.root = root;
    }

    public Pane getContenedorVid() {
        return contenedorVid;
    }

    public void setContenedorVid(Pane contenedorVid) {
        this.contenedorVid = contenedorVid;
    }

    public HBox getTurnoPuesto() {
        return turnoPuesto;
    }

    public void setTurnoPuesto(HBox turnoPuesto) {
        this.turnoPuesto = turnoPuesto;
    }

    public VBox getContenedorTurnoPuesto() {
        return contenedorTurnoPuesto;
    }

    public void setContenedorTurnoPuesto(VBox contenedorTurnoPuesto) {
        this.contenedorTurnoPuesto = contenedorTurnoPuesto;
    }

    public Label getTitulo() {
        return titulo;
    }

    public void setTitulo(Label titulo) {
        this.titulo = titulo;
    }

    public Label getReloj() {
        return reloj;
    }

    public void setReloj(Label reloj) {
        this.reloj = reloj;
    }

    public Scene getPantalla() {
        return Pantalla;
    }

    public void setPantalla(Scene Pantalla) {
        this.Pantalla = Pantalla;
    }

    public Label getTurno() {
        return turno;
    }

    public void setTurno(Label turno) {
        this.turno = turno;
    }

    public Label getPuesto() {
        return puesto;
    }

    public void setPuesto(Label puesto) {
        this.puesto = puesto;
    }

    public Label getHorarioAtencion() {
        return horarioAtencion;
    }

    public void setHorarioAtencion(Label horarioAtencion) {
        this.horarioAtencion = horarioAtencion;
    }

    public HBox getTitulos() {
        return titulos;
    }

    public void setTitulos(HBox titulos) {
        this.titulos = titulos;
    }
    
   
   
   
    
}
